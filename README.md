#BILLETERA VIRTUAL

#Trabajo practico integrador de programación III

#Nombre

PesoPay

#Descripcion del proyecto

El fin de este proyecto es crear desde cero y administrar una billetera virtual. Debido a la complejidad del mismo, tan solo se tendrán en cuenta la gestión de usuarios, saldos, pagos y cobros con tarjetas de débito y crédito, historial de transacciones, seguridad y soporte al cliente. 
Para esto se utilizaran cinco tablas en MySQl: Usuario, movimiento,tipo movimiento, cuenta y tarjeta.

Los limites del proyecto serán los siguientes:
- Cada usuario podrá tener una y sólo una cuenta
- Cada usuario podrá tener más de una tarjeta asociada a la billetera virtual, ya sea de crédito, débito y/o la generada por la billetera virtual 
- Los movimientos se registrarán conforme al monto, fecha, cuenta de destino y cuenta de origen, y calcularán el nuevo saldo en la cuenta del usuario.
- Los tipos de movimiento se distinguirán entre créditos, débitos y pagos.
- Una cuenta podrá estar asociada a un y sólo un usuario
- Una tarjeta podrá estar asociada a un y sólo un usuario
- 

#Stack Utilizado
Maven, Java, Spring Boot.

#Arquitectura
Clean Architecture - Principios SOLID - Patrones de Diseño

#DER Conceptual
[](https://drive.google.com/file/d/1Z6dmAaR0YtBleRQtxwRiv-ShVEZQbyae/view)


#Casos de uso
Caso de uso Nº1: Registrar Usuario

Caso de uso Nº2: Modificar Usuario

Caso de uso Nº3: Registrar Tarjeta

Caso de uso Nº4: Modificar Tarjeta

Caso de uso Nº5: Registrar Cuenta

Caso de uso Nº6: Modificar Cuenta

Caso de uso Nº7: Registrar Movimiento

Caso de uso Nº8: Modificar Movimiento


#Autores
Soria, José
Villagra, Julieta
Triador, Daniela 

